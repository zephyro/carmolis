<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->

	        <div class="main content">
		        <div class="container">
			        <h1>рейтинг участников</h1>

			        <div class="filter">

				        <ul>
					        <li>
						        <label class="filter__item">
							        <input type="radio" name="f1" value="" checked>
							        <span>За все время</span>
						        </label>
					        </li>
					        <li>
						        <label class="filter__item">
							        <input type="radio" name="f1" value="">
							        <span>Этот месяц</span>
						        </label>
					        </li>
					        <li>
						        <label class="filter__item">
							        <input type="radio" name="f1" value="">
							        <span>За апрель</span>
						        </label>
					        </li>
				        </ul>

				        <ul>
					        <li>
						        <label class="filter__item">
							        <input type="radio" name="f2" value="" checked>
							        <span>Все статусы</span>
						        </label>
					        </li>
					        <li>
						        <label class="filter__item">
							        <input type="radio" name="f2" value="">
							        <span>Ученик</span>
						        </label>
					        </li>
					        <li>
						        <label class="filter__item">
							        <input type="radio" name="f2" value="">
							        <span>Специалист</span>
						        </label>
					        </li>
					        <li>
						        <label class="filter__item">
							        <input type="radio" name="f2" value="">
							        <span>Эксперт</span>
						        </label>
					        </li>
					        <li>
						        <label class="filter__item">
							        <input type="radio" name="f2" value="">
							        <span>Эксперт+</span>
						        </label>
					        </li>
				        </ul>

			        </div>

			        <div class="table_responsive mb-50">
				        <table class="table">
					        <tr>
						        <th></th>
						        <th>Имя</th>
						        <th>Статус</th>
						        <th>Баллов</th>
					        </tr>

					        <tr>
						        <td>
							        <div class="avatar">
								        <img src="images/user__photo_01.png" class="img-fluid" alt="">
							        </div>
						        </td>
						        <td>Joyce Lynch</td>
						        <td><span class="status status_green"></span>Ученик</td>
						        <td>425</td>
					        </tr>

					        <tr>
						        <td>
							        <div class="avatar">
								        <img src="images/user__photo_02.png" class="img-fluid" alt="">
							        </div>
						        </td>
						        <td>Philip Little</td>
						        <td><span class="status status_red"></span>Эксперт+</td>
						        <td>235</td>
					        </tr>

					        <tr>
						        <td>
							        <div class="avatar">
								        <img src="images/user__photo_03.png" class="img-fluid" alt="">
							        </div>
						        </td>
						        <td>Marilyn Castro</td>
						        <td><span class="status status_yellow"></span>Эксперт</td>
						        <td>320</td>
					        </tr>

					        <tr>
						        <td>
							        <div class="avatar">
								        <img src="images/user__photo_04.png" class="img-fluid" alt="">
							        </div>
						        </td>
						        <td>Jacqueline Thomas</td>
						        <td><span class="status status_purple"></span>Специалист</td>
						        <td>125</td>
					        </tr>

					        <tr>
						        <td>
							        <div class="avatar">
								        <img src="images/user__photo_05.png" class="img-fluid" alt="">
							        </div>
						        </td>
						        <td>Angela Davidson</td>
						        <td><span class="status status_red"></span>Эксперт+</td>
						        <td>165</td>
					        </tr>

					        <tr>
						        <td>
							        <div class="avatar">
								        <img src="images/user__photo_06.png" class="img-fluid" alt="">
							        </div>
						        </td>
						        <td>Christine Keller</td>
						        <td><span class="status status_green"></span>Ученик</td>
						        <td>145</td>
					        </tr>

					        <tr>
						        <td>
							        <div class="avatar">
								        <img src="images/user__photo_07.png" class="img-fluid" alt="">
							        </div>
						        </td>
						        <td>Ron Swanson</td>
						        <td><span class="status status_yellow"></span>Эксперт</td>
						        <td>135</td>
					        </tr>

				        </table>
			        </div>

			        <ul class="pagination">
				        <li class="active"><a href="#">1</a></li>
				        <li><a href="#">2</a></li>
				        <li><span>...</span></li>
				        <li><a href="#">16</a></li>
				        <li><a href="#">>></a></li>
			        </ul>

		        </div>
	        </div>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
