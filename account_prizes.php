<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
	        <div class="user">
		        <div class="container">
			        <h1>ПРИЗЫ И ПРИВИЛЕГИИ</h1>
			        <div class="user__text">
				        Идейные соображения высшего порядка, а также рамки и место обучения кадров позволяет выполнять важные задания по разработке существенных финансовых и административных условий. Задача организации, в особенности же постоянное информационно-пропагандистское обеспечение нашей деятельности.
			        </div>
		        </div>
	        </div>

	        <section class="wave wave_prize">
		        <div class="container">

			        <div class="wave__title">ПРИЗЫ<br/>И ПРИВИЛЕГИИ</div>

			        <div class="prize__row">

				        <div class="prize">
					        <div class="prize__wrap">
						        <div class="prize__date">январь 2019</div>
						        <div class="prize__event">УЧАСТИЕ В КОНФЕРЕНЦИИ</div>
						        <div class="prize__image">
							        <img src="img/icon__prize_01.png" class="img-fluid" alt="">
						        </div>
						        <div class="prize__status">СТАТУС «ЭКСПЕРТ»</div>
						        <div class="prize__name"><span>15 САМЫХ АКТИВНЫХ</span></div>
					        </div>
				        </div>

				        <div class="prize">
					        <div class="prize__wrap">
						        <div class="prize__date">~ май 2019</div>
						        <div class="prize__event">КОНФЕРЕНЦИЯ В ВЕНЕ</div>
						        <div class="prize__image">
							        <img src="img/icon__prize_02.png" class="img-fluid" alt="">
						        </div>
						        <div class="prize__status">СТАТУС «ЭКСПЕРТ+» И «ЭКСПЕРТ»</div>
						        <div class="prize__name"><span>15 САМЫХ АКТИВНЫХ</span></div>
					        </div>
				        </div>

				        <div class="prize">
					        <div class="prize__wrap">
						        <div class="prize__date">раз в месяц</div>
						        <div class="prize__event">2 билета в театр</div>
						        <div class="prize__image">
							        <img src="img/icon__prize_03.png" class="img-fluid" alt="">
						        </div>
						        <div class="prize__status">СТАТУС «УЧЕНИК»</div>
						        <div class="prize__name"><span>5 САМЫХ АКТИВНЫХ</span></div>
					        </div>
				        </div>

				        <div class="prize">
					        <div class="prize__wrap">
						        <div class="prize__date">раз в месяц</div>
						        <div class="prize__event">Памятные украшения</div>
						        <div class="prize__image">
							        <img src="img/icon__prize_04.png" class="img-fluid" alt="">
						        </div>
						        <div class="prize__status">СТАТУС «СПЕЦИАЛИСТ»</div>
						        <div class="prize__name"><span>10 САМЫХ АКТИВНЫХ</span></div>
					        </div>
				        </div>

			        </div>

		        </div>
	        </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
