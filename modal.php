<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->

	        <div class="main content">
		        <div class="container">
			        <h1>Модальные окна</h1>

			        <ul class="test_list">

				        <li><a class="btn_modal" href="#enter">Вход</a></li>
				        <li><a class="btn_modal" href="#reg">Регистрация</a></li>
				        <li><a class="btn_modal" href="#recovery">Восстановление пароля</a></li>
				        <li><a class="btn_modal" href="#success_recovery">Успешно восстановлен пароль</a></li>
				        <li><a class="btn_modal" href="#new_pass">Новый пароль</a></li>
				        <li><a class="btn_modal" href="#success">Успешно</a></li>
				        <li><a class="btn_modal" href="#feedback">Обратная связь</a></li>
				        <li><a class="btn_modal" href="#success_message">Успешная отправка</a></li>
				        <li><a class="btn_modal" href="#write">Написать администрации</a></li>
				        <li><a class="btn_modal" href="#success_write">Успешно отправлено сообщение</a></li>
			        </ul>

		        </div>
	        </div>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Вход -->
        <div class="hide">
	        <a href="#enter" class="open_reg btn_modal"></a>
	        <div class="modal" id="enter">
		        <div class="modal__heading">
			        <div class="modal__heading_title">Вход</div>
			        <span>или</span>
			        <a href="#">зарегистироваться</a>
		        </div>
		        <div class="modal__body">
			        <form class="form">

				        <div class="form_group">
					        <div class="modal__row">
						        <div class="modal__row_left">
							        <label class="form_label"><span>Е-MAIL</span></label>
						        </div>
						        <div class="modal__row_right">
							        <input type="text" class="form_control" name="" placeholder="">
						        </div>
					        </div>
				        </div>

				        <div class="form_group">
					        <div class="modal__row">
						        <div class="modal__row_left">
							        <label class="form_label"><span>ПАРОЛЬ</span></label>
						        </div>
						        <div class="modal__row_right">
							        <input type="text" class="form_control" name="" placeholder="">
						        </div>
					        </div>
				        </div>

				        <div class="form_group mb-20">
					        <div class="modal__row">
						        <div class="modal__row_left"></div>
						        <div class="modal__row_right">
							        <label class="form_checkbox">
								        <input type="checkbox" name="" value="">
								        <span>Запомнить меня</span>
							        </label>
							        <a href="#" class="modal__recover">восстановить пароль</a>
						        </div>
					        </div>
				        </div>

				        <div class="form_group mb-20">
					        <div class="modal__row">
						        <div class="modal__row_left"></div>
						        <div class="modal__row_right">
									<button type="submit" class="btn btn_shadow">Войти</button>
						        </div>
					        </div>
				        </div>

				        <div class="form_group">
					        <div class="modal__row">
						        <div class="modal__row_left"></div>
						        <div class="modal__row_right">
							        <div class="modal__social">
								        <div class="modal__social_title">Войти через аккаунт социальной сети</div>
								        <ul>
									        <li><a href="#"><i class="fa fa-google"></i></a></li>
									        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
									        <li><a href="#"><i class="fa fa-vk"></i></a></li>
									        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
								        </ul>
							        </div>
						        </div>
					        </div>
				        </div>

			        </form>
		        </div>
		        <div class="modal__image">
			        <img src="img/modal__image_01.svg" class="img-fluid" alt="">
		        </div>

	        </div>
        </div>
        <!-- -->

        <!-- Регистрация -->
        <div class="hide">
	        <a href="#reg" class="open_reg btn_modal"></a>
	        <div class="modal" id="reg">
		        <div class="modal__heading">
			        <div class="modal__heading_title">Регистрация</div>
			        <span>или</span>
			        <a href="#">вход на сайт</a>
		        </div>
		        <div class="modal__body">
			        <form class="form">

				        <div class="form_group">
					        <div class="modal__row">
						        <div class="modal__row_left">
							        <label class="form_label"><span>Е-MAIL</span></label>
						        </div>
						        <div class="modal__row_right">
							        <input type="text" class="form_control" name="" placeholder="">
						        </div>
					        </div>
				        </div>

				        <div class="form_group">
					        <div class="modal__row">
						        <div class="modal__row_left">
							        <label class="form_label"><span>ПАРОЛЬ</span></label>
						        </div>
						        <div class="modal__row_right">
							        <input type="text" class="form_control" name="" placeholder="">
						        </div>
					        </div>
				        </div>

				        <div class="form_group">
					        <div class="modal__row">
						        <div class="modal__row_left">
							        <label class="form_label"><span>Капча</span></label>
						        </div>
						        <div class="modal__row_right">
							       <div class="form_captcha">
								       <img src="images/captcha.png" class="img-fluid" alt="">
							       </div>
						        </div>
					        </div>
				        </div>

				        <div class="form_group mb-20">
					        <div class="modal__row">
						        <div class="modal__row_left"></div>
						        <div class="modal__row_right">
							        <label class="form_checkbox">
								        <input type="checkbox" name="" value="">
								        <span>Запомнить меня</span>
							        </label>
							        <a href="#" class="modal__recover">восстановить пароль</a>
						        </div>
					        </div>
				        </div>

				        <div class="form_group mb-20">
					        <div class="modal__row">
						        <div class="modal__row_left"></div>
						        <div class="modal__row_right">
							        <button type="submit" class="btn btn_shadow">Регистрация</button>
						        </div>
					        </div>
				        </div>

				        <div class="form_group">
					        <div class="modal__row">
						        <div class="modal__row_left"></div>
						        <div class="modal__row_right">
							        <div class="modal__social">
								        <div class="modal__social_title">Войти через аккаунт социальной сети</div>
								        <ul>
									        <li><a href="#"><i class="fa fa-google"></i></a></li>
									        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
									        <li><a href="#"><i class="fa fa-vk"></i></a></li>
									        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
								        </ul>
							        </div>
						        </div>
					        </div>
				        </div>

			        </form>
		        </div>
		        <div class="modal__image">
			        <img src="img/modal__image_01.svg" class="img-fluid" alt="">
		        </div>

	        </div>
        </div>
        <!-- -->

        <!-- Восстановление пароля -->
        <div class="hide">
	        <a href="#recovery" class="open_recovery btn_modal"></a>
	        <div class="modal" id="recovery">
		        <div class="modal__heading">
			        <div class="modal__heading_title">Восстановление пароля</div>
		        </div>
		        <div class="modal__body">
			        <form class="form">

				        <div class="form_group mb-30">
					        <div class="modal__row">
						        <div class="modal__row_left">
							        <label class="form_label"><span>ВАШ ЕMAIL</span></label>
						        </div>
						        <div class="modal__row_right">
							        <input type="text" class="form_control" name="" placeholder="">
						        </div>
					        </div>
				        </div>
				        <div class="form_group mb-20">
					        <div class="modal__row">
						        <div class="modal__row_left"></div>
						        <div class="modal__row_right">
							        <button type="submit" class="btn btn_shadow">восстановить</button>
						        </div>
					        </div>
				        </div>
				        <div class="form_group">
					        <div class="modal__row">
						        <div class="modal__row_left"></div>
						        <div class="modal__row_right text-center">
							        <span class="color-blue">или</span> <a href="#">Зарегистрироваться</a>
						        </div>
					        </div>
				        </div>

			        </form>
		        </div>

	        </div>
        </div>
        <!-- -->

        <!-- Успешно Восстановление пароля -->
        <div class="hide">
	        <a href="#success_recovery" class="open_success_recovery btn_modal"></a>
	        <div class="modal success" id="success_recovery">
		        <div class="modal__success">
			        <div class="modal__success_image">
				        <img src="img/modal__success.svg" class="img-fluid" alt="">
			        </div>
			        <div class="modal__success_title">успешно!</div>
			        <div class="modal__success_text">на ваш e-mail отправлена ссылка<br/>для восстановления пароля</div>
			        <div class="text-center">
				        <a href="#" class="btn btn_xl btn_shadow">Войти на сайт</a>
			        </div>
		        </div>
	        </div>
        </div>
        <!-- -->

        <!-- Новый пароль -->
        <div class="hide">
	        <a href="#new_pass" class="open_new_pas btn_modal"></a>
	        <div class="modal" id="new_pass">
		        <div class="modal__heading">
			        <div class="modal__heading_title">Введите Ваш новый пароль</div>
		        </div>
		        <div class="modal__body">
			        <form class="form">

				        <div class="form_group mb-30">
					        <div class="modal__row">
						        <div class="modal__row_left">
							        <label class="form_label"><span>Пароль</span></label>
						        </div>
						        <div class="modal__row_right">
							        <input type="password" class="form_control" name="" placeholder="*********">
						        </div>
					        </div>
				        </div>
				        <div class="form_group mb-30">
					        <div class="modal__row">
						        <div class="modal__row_left">
							        <label class="form_label"><span>Пароль повторно</span></label>
						        </div>
						        <div class="modal__row_right">
							        <input type="password" class="form_control" name="" placeholder="*********">
						        </div>
					        </div>
				        </div>
				        <div class="form_group mb-20">
					        <div class="modal__row">
						        <div class="modal__row_left"></div>
						        <div class="modal__row_right">
							        <button type="submit" class="btn btn_shadow">сохранить</button>
						        </div>
					        </div>
				        </div>

			        </form>
		        </div>

	        </div>
        </div>
        <!-- -->

        <!-- Успешно -->
        <div class="hide">
	        <a href="#success" class="open_success btn_modal"></a>
	        <div class="modal success" id="success">
		        <div class="modal__success">
			        <div class="modal__success_image">
				        <img src="img/modal__success.svg" class="img-fluid" alt="">
			        </div>
			        <div class="modal__success_title">успешно!</div>
			        <div class="modal__success_text">Ваш новый пароль успешно сохранен</div>
			        <div class="text-center">
				        <a href="#" class="btn btn_xl btn_shadow">Войти на сайт</a>
			        </div>
		        </div>
	        </div>
        </div>
        <!-- -->

        <!-- Обратная связь -->
        <div class="hide">
	        <a href="#feedback" class="open_feedback btn_modal"></a>
	        <div class="modal" id="feedback">
		        <div class="modal__heading">
			        <div class="modal__heading_title">Обратная связь</div>
		        </div>
		        <div class="modal__body">
			        <form class="form">

				        <div class="form_group">
					        <div class="modal__row">
						        <div class="modal__row_left">
							        <label class="form_label"><span>Е-MAIL для ответа</span></label>
						        </div>
						        <div class="modal__row_right">
							        <input type="text" class="form_control" name="" placeholder="">
						        </div>
					        </div>
				        </div>
				        <div class="form_group mb-30">
					        <div class="modal__row">
						        <div class="modal__row_left">
							        <label class="form_label form_label_top"><span>Ваше сообщение</span></label>
						        </div>
						        <div class="modal__row_right">
							        <textarea class="form_control" name="" placeholder="" rows="5"></textarea>
						        </div>
					        </div>
				        </div>
				        <div class="form_group">
					        <div class="modal__row">
						        <div class="modal__row_left"></div>
						        <div class="modal__row_right">
							        <button type="submit" class="btn btn_shadow">отправить</button>
						        </div>
					        </div>
				        </div>

			        </form>
		        </div>

	        </div>
        </div>
        <!-- -->


        <!-- Успешная отправка -->
        <div class="hide">
	        <a href="#success_message" class="open_success_message btn_modal"></a>
	        <div class="modal success" id="success_message">
		        <div class="modal__success">
			        <div class="modal__success_image">
				        <img src="img/modal__success.svg" class="img-fluid" alt="">
			        </div>
			        <div class="modal__success_title">Ваше сообщение<br/>отправлено!</div>
			        <div class="modal__success_text">С уважением, CARMOLIS.RU</div>
			        <div class="text-center">
				        <a href="#" class="btn btn_xl btn_shadow">На главную</a>
			        </div>
		        </div>
	        </div>
        </div>
        <!-- -->

        <!-- Написать администрации -->
        <div class="hide">
	        <a href="#write" class="open_write btn_modal"></a>
	        <div class="modal" id="write">
		        <div class="modal__heading">
			        <div class="modal__heading_title">Написать администрации</div>
		        </div>
		        <div class="modal__body">
			        <form class="form">

				        <div class="form_group mb-30">
					        <div class="modal__row">
						        <div class="modal__row_left">
							        <label class="form_label form_label_top"><span>Ваш отзыв</span></label>
						        </div>
						        <div class="modal__row_right">
							        <textarea class="form_control" name="" placeholder="" rows="5"></textarea>
						        </div>
					        </div>
				        </div>
				        <div class="form_group">
					        <div class="modal__row">
						        <div class="modal__row_left"></div>
						        <div class="modal__row_right">
							        <button type="submit" class="btn btn_shadow">оставить отзыв</button>
						        </div>
					        </div>
				        </div>

			        </form>
		        </div>

	        </div>
        </div>
        <!-- -->

        <!-- Успешно отправлено администрации -->
        <div class="hide">
	        <a href="#success_write" class="open_success_write btn_modal"></a>
	        <div class="modal success" id="success_write">
		        <div class="modal__success">
			        <div class="modal__success_image">
				        <img src="img/modal__success_reply.png" class="img-fluid" alt="">
			        </div>
			        <div class="modal__success_title mb-30">
				        ВАШ ОТЗЫВ<br/>
				        ОТПРАВЛЕН CARMOLIS.RU<br/>
				        СПАСИБО!
			        </div>
			        <div class="text-center">
				        <a href="#" class="btn btn_xl btn_shadow">на главную</a>
			        </div>
		        </div>
	        </div>
        </div>
        <!-- -->

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>

