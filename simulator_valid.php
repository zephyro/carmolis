<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->

	        <div class="simulator__heading">
		        <div class="container">
			        <h1>ТРЕНАЖЕР ПРОДАЖ</h1>
			        <ul class="simulator__scale">
				        <li class="simulator__scale_yellow"></li>
				        <li class="simulator__scale_red"></li>
				        <li class="simulator__scale_brown"></li>
			        </ul>
		        </div>
	        </div>


	        <div class="simulator simulator_valid" style="background: url('img/trainer__bg.png') 50% 50% no-repeat; background-size: cover">

		        <div class="container">

			        <div class="simulator__time"><span>2:10</span></div>

			        <div class="simulator__row">

				        <div class="simulator__left">

					        <!-- Test -->
					        <div class="simulator__test">

						        <div class="simulator__question">Здравствуйте! Посоветуйте что-нибудь от простуды. Kак-то не важно себя чувствую. Как бы не заболеть.</div>

						        <div class="simulator__response">

							        <label class="simulator__option">
								        <input type="radio" name="option" value="">
								        <span data-option="1">Здравствуйте! Я знаю, что Вам может помочь, у нас как раз есть хорошие Кармолис® Капли.</span>
							        </label>

							        <label class="simulator__option">
								        <input type="radio" name="option" value="">
								        <span data-option="2">Здравствуйте! Я постараюсь Вам помочь. Какие симптомы Вас беспокоят, когда они появились? Чем-то уже пользовались?</span>
							        </label>

							        <label class="simulator__option">
								        <input type="radio" name="option" value="">
								        <span data-option="3">Сейчас все болеют, наверное эпидемия. Все чихают, кашляют - как же тут не заболеть! Я Вам что-нибудь сейчас подберу.</span>
							        </label>

						        </div>

					        </div>
					        <!-- -->

					        <!-- Result -->
					        <div class="simulator__result simulator__result_good active">
						        <div class="simulator__result_image">
							        <img src="img/trainer__smile_ok.png" class="img-fluid" alt="">
						        </div>
						        <div class="simulator__result_text">Ответ профессиональный, правильно подобраны вопросы, направленные на выяснение потребности клиента.</div>
						        <div class="text-center">
							        <a href="#" class="btn btn_sm btn_next">ПРОДОЛЖИТЬ</a>
						        </div>
					        </div>

					        <div class="simulator__result simulator__result_valid">
						        <div class="simulator__result_image">
							        <img src="img/trainer__smile_sad.png" class="img-fluid" alt="">
						        </div>
						        <div class="simulator__result_text">Ответ непрофессиональный. Вы рискуете потерять клиента. Попробуйте ответить на вопрос еще раз.</div>
						        <div class="text-center">
							        <a href="#" class="btn btn_sm btn_next">ПРОДОЛЖИТЬ</a>
						        </div>
					        </div>

					        <div class="simulator__result simulator__result_invalid">
						        <div class="simulator__result_image">
							        <img src="img/trainer__smile_evil.png" class="img-fluid" alt="">
						        </div>
						        <div class="simulator__result_text">Вы не владеете информацией о препарате и не умеете работать с возражениями клиента.</div>
						        <div class="text-center">
							        <a href="#" class="btn btn_rose btn_sm btn_next">НАЧАТЬ СНАЧАЛА</a>
						        </div>
					        </div>
					        <!-- -->

				        </div>

				        <div class="simulator__right">
					        <div class="simulator__user simulator__user_base">
						        <img src="img/trainer__user_base.png" class="img-fluid" alt="">
					        </div>
					        <div class="simulator__user simulator__user_good">
						        <img src="img/trainer__user_good.png" class="img-fluid" alt="">
					        </div>
					        <div class="simulator__user simulator__user_valid">
						        <img src="img/trainer__user_sad.png" class="img-fluid" alt="">
					        </div>
					        <div class="simulator__user simulator__user_invalid">
						        <img src="img/trainer__user_evil.png" class="img-fluid" alt="">
					        </div>
				        </div>

			        </div>

		        </div>

	        </div>

            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
