<header class="header">
    <div class="container">

        <a href="#" class="header__logo">
            <div class="header__logo_img">
                <img src="img/logo.svg" class="img-fluid" alt="">
            </div>
            <div class="header__logo_text">
                <img src="img/logo__text.svg" class="img-fluid" alt="">
            </div>
        </a>

        <a href="#" class="header__toggle">
            <span></span>
            <span></span>
            <span></span>
        </a>

        <a href="#" class="header__account">
            <svg class="ico-svg" viewBox="0 0 18 18" xmlns="http://www.w3.org/2000/svg">
                <use xlink:href="img/sprite_icons.svg#icon__user_account" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
            </svg>
        </a>

        <nav class="header__nav">
            <ul>
                <li class="active"><a href="#"><span>НОВОСТИ</span></a></li>
                <li><a href="#"><span>ПРАВИЛА</span></a></li>
                <li><a href="#"><span>ПРИЗЫ</span></a></li>
                <li><a href="#"><span>РЕЙТИНГ УЧАСТНИКОВ</span></a></li>
                <li><a href="#"><span>КОНКУРС</span></a></li>
            </ul>
            <a class="header__learn">обучение</a>
        </nav>

    </div>
</header>