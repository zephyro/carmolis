<footer class="footer">
    <div class="container">
        <div class="footer__row">

            <div class="footer__info">

                <a href="#" class="footer__logo">
                    <div class="footer__logo_img">
                        <img src="img/footer__logo.svg" class="img-fluid" alt="">
                    </div>
                </a>

                <div class="footer__info_text">© Copyright ООО «САНТА СНГ» 2018, РФ, 143444, Московская обл., г. Красногорск, мкр. Опалиха, ул. Геологов, д. 6, офис 4. </div>

                <div class="footer__info_dev">Разработка портала by <a href="#">Hix.One.</a></div>

            </div>

            <div class="footer__contact">
                <div class="footer__contact_left">
                    <ul class="footer__social">
                        <li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" title=""><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#" title=""><i class="fa fa-vk"></i></a></li>
                        <li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" title=""><i class="fa fa-odnoklassniki"></i></a></li>
                        <li><a href="#" title=""><i class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
                <div class="footer__contact_right">

                    <ul class="footer__nav">
                        <li><a href="#">НОВОСТИ</a></li>
                        <li><a href="#">ПРАВИЛА</a></li>
                        <li><a href="#">ПРИЗЫ</a></li>
                        <li><a href="#">РЕЙТИНГ УЧАСТНИКОВ</a></li>
                        <li><a href="#">КОНКУРС</a></li>
                    </ul>

                    <div class="footer__contact_row">

                        <div class="footer__meta">
                            <ul>
                                <li><a href="#">Взрослым</a></li>
                                <li><a href="#">Детям</a></li>
                                <li><a href="#">Специалистам</a></li>
                                <li><a href="#">Что болит</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">Препараты</a></li>
                                <li><a href="#">Где купить?</a></li>
                                <li><a href="#">Про Кармолис</a></li>
                            </ul>
                        </div>

                        <div class="footer__button">
                            <a href="#" class="btn btn_border_blue btn_modal">Обратная связь</a>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>
</footer>