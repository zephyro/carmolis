// SVG IE11 support
svg4everybody();

// Mobile Navigation
$('.header__toggle').on('click touchstart', function(e) {
    e.preventDefault();
    $(this).closest('.page').toggleClass('open_nav');
});


$(".btn_modal").fancybox({
    'padding'    : 0
});


var comments = new Swiper('.comments__slider', {
    loop: true,
    pagination: {
        el: '.swiper-pagination'
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    }
});



var gallery = new Swiper('.gallery__slider', {
    loop: true,
    pagination: {
        el: '.swiper-pagination'
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    }
});


$('.form_file input[type="file"]').on('change', function(e) {
    var box = $(this).closest('.form_file');
    var str = $(this).val();

    if (str.lastIndexOf('\\')){
        var i = str.lastIndexOf('\\')+1;
    }
    else{
        var i = str.lastIndexOf('/')+1;
    }
    var filename = str.slice(i);
    box.find('.form_file__text').text(filename);
});



$('.simulator input[name="option"]').on('change', function(e) {
    var option = parseInt($(this).val());
    console.log(option);


    switch (option) {
        case 0:
            $('.simulator').find('.simulator__user').removeClass('active');
            $('.simulator').find('.simulator__user_invalid').addClass('active');
            break;
        case 1:
            $('.simulator').find('.simulator__user').removeClass('active');
            $('.simulator').find('.simulator__user_valid').addClass('active');
            break;
        case 2:
            $('.simulator').find('.simulator__user').removeClass('active');
            $('.simulator').find('.simulator__user_good').addClass('active');
            break;
        default:
            $('.simulator').find('.simulator__user').removeClass('active');
            $('.simulator').find('.simulator__user_base').addClass('active');
    }
});
