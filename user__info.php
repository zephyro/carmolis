<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->

	        <div class="main">
		        <div class="container">

			        <div class="profile__heading">
				        <div class="profile__heading_title">ПУШКОВ АЛЕКСАНДР</div>
				        <div class="profile__heading_info">Баллов: <span class="color-blue">156</span></div>
			        </div>

			        <div class="profile">

				        <div class="profile__nav">
					        <div class="profile__nav_title">Меню</div>
					        <ul>
						        <li class="active"><a href="#">Общая информация</a></li>
						        <li><a href="#">Баллы</a></li>
						        <li><a href="#">Конкурс</a></li>
						        <li><a href="#">История входа</a></li>
						        <li><a href="#" class="color-red">Выход</a></li>
					        </ul>
				        </div>

				        <div class="profile__content">

					        <div class="form_group form_group_inline">
						        <div class="form_group_input offset">
							        <h3>ОСНОВНАЯ ИНФОРМАЦИЯ</h3>
						        </div>
					        </div>

					        <div class="form_group form_group_inline">
						        <label class="inline_label"><span>E-mail</span></label>
						        <div class="form_group_input">
							        <div class="profile_text">654686@gmail.com | <span class="color-blue">Подтвержлден</span></div>
						        </div>
					        </div>

					        <div class="form_group form_group_inline">
						        <label class="inline_label"><span>Имя и фамилия</span></label>
						        <div class="form_group_input">
							        <input type="text" class="form_control" name="" value="Карвелис Александр" placeholder="">
						        </div>
					        </div>

					        <div class="form_group form_group_inline">
						        <label class="inline_label"><span>Фото пользователя</span></label>
						        <div class="form_group_input">

							        <label class="form_file">
								        <input class="form_file__input" type="file" name="" placeholder="" value="">
								        <span class="form_file__icon"></span>
								        <span class="form_file__text"><span></span></span>
								        <span class="form_file__btn">Загрузить</span>
							        </label>

						        </div>
					        </div>

					        <div class="form_group form_group_inline mb-30">

						        <div class="form_group_input offset">

							        <div class="profile__photo">
								        <div class="profile__photo_item">
									        <a href="#">
										        <img src="images/user_photo.jpg" class="img-fluid" alt="">
									        </a>
								        </div>
								        <div class="profile__photo_text">
									        <a class="profile__photo_remove" href="#">удалить фото</a>
								        </div>
							        </div>
						        </div>
					        </div>

					        <div class="form_group form_group_inline mb-30">
						        <label class="inline_label"><span>Привязанные аккаунты</span></label>
						        <div class="form_group_input">

							        <ul class="profile__social">
								        <li><a href="#">привязать facebook</a> <span>facebook.com/yuri.k</span></li>
								        <li><a href="#">привязать google</a> <span>google.com/account5</span></li>
								        <li><a href="#">привязать vk.com</a> <span>vk.com/yuri.k</span></li>
							        </ul>
						        </div>
					        </div>

					        <div class="form_group form_group_inline mb-30">
						        <label class="inline_label"><span>Сертификат</span></label>
						        <div class="form_group_input">

							        <label class="form_file">
								        <input class="form_file__input" type="file" name="" placeholder="" value="">
								        <span class="form_file__icon"></span>
								        <span class="form_file__text"><span></span></span>
								        <span class="form_file__btn">Загрузить</span>
							        </label>

							        <div class="profile__confirmation">подтвержден 11.08.2019, <a href="#">ссылка на изображение</a></div>

						        </div>
					        </div>

					        <div class="form_group form_group_inline">
						        <label class="inline_label"><span>Кто пригласил</span></label>
						        <div class="form_group_input">
							        <div class="profile_text"><span class="color-blue">34304@gmail.com | Пушков Андрей</span></div>
						        </div>
					        </div>

					        <div class="form_group form_group_inline mb-50">
						        <label class="inline_label"><span>Статус</span></label>
						        <div class="form_group_input">
							        <div class="profile_text"><span class="color-blue">Новичок</span></div>
						        </div>
					        </div>

					        <div class="form_group form_group_inline">
						        <div class="form_group_input offset">
							        <button type="submit" class="btn btn_xl">сохранить</button>
						        </div>
					        </div>

					        <div class="profile__divider"></div>

					        <div class="form_group form_group_inline">
						        <div class="form_group_input offset">
							        <h4>Установить новый пароль</h4>
						        </div>
					        </div>

					        <div class="form_group form_group_inline">
						        <label class="inline_label"><span>Пароль</span></label>
						        <div class="form_group_input">
							        <input type="password" class="form_control" name="" value="" placeholder="***********">
						        </div>
					        </div>

					        <div class="form_group form_group_inline mb-50">
						        <label class="inline_label"><span>Пароль повторно</span></label>
						        <div class="form_group_input">
							        <input type="password" class="form_control" name="" value="" placeholder="***********">
						        </div>
					        </div>

					        <div class="form_group form_group_inline">
						        <div class="form_group_input offset">
							        <button type="submit" class="btn btn_xl">сохранить</button>
						        </div>
					        </div>

				        </div>

			        </div>

		        </div>
	        </div>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
