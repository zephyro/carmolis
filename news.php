<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->

	        <div class="main">
		        <div class="container">
			        <h1>ПРИЗЫ И ПРИВИЛЕГИИ</h1>

			        <article class="news">
				        <h2><a href="#">Конкурс завершён! Поздравляем победителей!</a></h2>
				        <div class="news__image">
					        <a href="#">
						        <img src="images/news_image.jpg" class="img-fluid" alt="">
					        </a>
				        </div>
				        <div class="news__intro">Завершилось главное мировое событие – Чемпионат Мира по футболу-2018! Подведены итоги и нашего футбольного Конкурса прогнозов «Кармолис и АСНА – болеем классно!», начинается самый важный и приятный этап – чествование наших победителей! Мы впечатлены всероссийским охватом нашего Конкурса – призы отправятся к своим обладателям, проживающим от Крыма до...</div>
			        </article>

			        <article class="news">
				        <h2><a href="#">Конкурс завершён! Поздравляем победителей!</a></h2>
				        <div class="news__image">
					        <a href="#">
						        <img src="images/news_image.jpg" class="img-fluid" alt="">
					        </a>
				        </div>
				        <div class="news__intro">Завершилось главное мировое событие – Чемпионат Мира по футболу-2018! Подведены итоги и нашего футбольного Конкурса прогнозов «Кармолис и АСНА – болеем классно!», начинается самый важный и приятный этап – чествование наших победителей! Мы впечатлены всероссийским охватом нашего Конкурса – призы отправятся к своим обладателям, проживающим от Крыма до...</div>
			        </article>

			        <ul class="pagination">
				        <li class="active"><a href="#">1</a></li>
				        <li><a href="#">2</a></li>
				        <li><span>...</span></li>
				        <li><a href="#">16</a></li>
				        <li><a href="#">>></a></li>
			        </ul>

		        </div>
	        </div>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
