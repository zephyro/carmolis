<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
            <div class="promo">
                <div class="container">
                    <div class="promo__wrap">
                        <div class="promo__left">
                            <i>
                                <img src="img/quote_left.svg" class="img-fluid">
                            </i>
                            <span>привет!</span>
                        </div>
                        <div class="promo__right">
                            <i>
                                <img src="img/quote_right.svg" class="img-fluid">
                            </i>
                            <span>В игровой форме обучим и предоставим возможность выиграть призы </span>
                        </div>
    
                        <div class="promo__image">
                            <img src="img/people.svg" class="img-fluid" alt="">
                        </div>
                    </div>
                    <h1>Образовательный портал</h1>
                    <div class="promo__subtitle">для провизоров и фармацевтов</div>
                </div>
            </div>
            
            <section class="advantage">
                <div class="container">
                    <ul class="advantage__row">
                        <li>
                            <div class="advantage__image">
                                <img src="img/advantage__icon_01.svg" class="img-fluid" alt="">
                            </div>
                            <span>Повысь уровень профессиональных знаний</span>
                        </li>
                        <li>
                            <div class="advantage__image">
                                <img src="img/advantage__icon_02.svg" class="img-fluid" alt="">
                            </div>
                            <span>Узнай секреты Кармолис</span>
                        </li>
                        <li>
                            <div class="advantage__image">
                                <img src="img/advantage__icon_03.svg" class="img-fluid" alt="">
                            </div>
                            <span>Стань экспертом Кармолис</span>
                        </li>
                        <li>
                            <div class="advantage__image">
                                <img src="img/advantage__icon_04.svg" class="img-fluid" alt="">
                            </div>
                            <span>Прояви творческий потенциал</span>
                        </li>
                    </ul>
                </div>
            </section>
            
            <section class="profit">
                <div class="container">
                    <div class="profit__heading">Что получают участники?</div>
                    <ul class="profit__list">
                        <li>
                            <a href="#">
                                <span>Обучающие МОДУЛИ</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>Свежие новости фарм рынка в одном месте</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>БАЛЛЫ и<br/>статусы</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>Призы и привелегии</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </section>
            
            <section class="become">
                <div class="container">
                    <div class="become__heading">Как стать участником?</div>
                    <ul class="become__list">
                        <li>
                            <div class="become__image">
                                <img src="img/become__icon_01.svg" class="img-fluid" alt="">
                            </div>
                            <span>Зарегистрируйся</span>
                        </li>
                        <li>
                            <div class="become__image">
                                <img src="img/become__icon_02.svg" class="img-fluid" alt="">
                            </div>
                            <span>Пройди обучающий модуль</span>
                        </li>
                        <li>
                            <div class="become__image">
                                <img src="img/become__icon_03.svg" class="img-fluid" alt="">
                            </div>
                            <span>Заработай статус</span>
                        </li>
                        <li>
                            <div class="become__image">
                                <img src="img/become__icon_04.svg" class="img-fluid" alt="">
                            </div>
                            <span>Получи свой Приз</span>
                        </li>
                    </ul>
                    <div class="text-center">
                        <a href="#" class="btn btn_lg btn_shadow">Начать обучение</a>
                    </div>
                </div>
            </section>
            
            <section class="comments">
                <div class="container">
                    <div class="comments__heading"><span>Отзывы участников</span></div>
    
                    <div class="comments__slider swiper-container">
                        <div class="swiper-wrapper">
    
                            <div class="swiper-slide">
                                <div class="comments__photo">
                                    <img src="images/man_photo_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="comments__author">Александр пушков</div>
                                <div class="comments__text">Шикарное обучение. Отличные призы, зачет! Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет! Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет!</div>
                                <a href="#" class="comments__social comments__social_vk"><i class="fa fa-vk"></i></a>
                            </div>
    
                            <div class="swiper-slide">
                                <div class="comments__photo">
                                    <img src="images/man_photo_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="comments__author">Александр пушков</div>
                                <div class="comments__text">Шикарное обучение. Отличные призы, зачет! Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет! Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет!</div>
                                <a href="#" class="comments__social comments__social_vk"><i class="fa fa-vk"></i></a>
                            </div>
    
                            <div class="swiper-slide">
                                <div class="comments__photo">
                                    <img src="images/man_photo_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="comments__author">Александр пушков</div>
                                <div class="comments__text">Шикарное обучение. Отличные призы, зачет! Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет! Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет!</div>
                                <a href="#" class="comments__social comments__social_vk"><i class="fa fa-vk"></i></a>
                            </div>
    
                            <div class="swiper-slide">
                                <div class="comments__photo">
                                    <img src="images/man_photo_01.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="comments__author">Александр пушков</div>
                                <div class="comments__text">Шикарное обучение. Отличные призы, зачет! Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет! Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет!Шикарное обучение. Отличные призы, зачет!</div>
                                <a href="#" class="comments__social comments__social_vk"><i class="fa fa-vk"></i></a>
                            </div>
                            
                            
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                        <!-- Add Arrows -->
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                    
                    <div class="text-center">
                        <a href="#" class="btn btn_border_blue btn_modal">Добавить отзыв</a>
                    </div>
                    
                </div>
            </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
