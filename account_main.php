<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->
            
	        <div class="user">
		        <div class="container">
			        <div class="user__row">
				        <div class="user__left">
					        <div class="user__photo">
								<img src="images/man_lg.jpg" class="img-fluid" alt="">
					        </div>
				        </div>
				        <div class="user__center">
					        <div class="user__title">МОИ БАЛЛЫ</div>
					        <div class="user__rate">Баллов: <span>156</span></div>
					        <a class="user__history" href="#" >История начисления баллов</a>
				        </div>
				        <div class="user__right">
					        <div class="user__title">МОЙ СТАТУС</div>
					        <div class="user__status">Эксперт</div>
					        <div class="user__next">До следующего статуса <strong class="color_blue">Эксперт +</strong> <br/>осталось баллов: <span class="color_blue">210</span></div>
					        <div class="user__completed">Пройденных обучений: <span class="color_blue">6</span></div>
				        </div>
			        </div>
		        </div>
	        </div>

	        <section class="wave">
		        <div class="container">

			        <div class="wave__subtitle">прохождение только один раз</div>
			        <div class="wave__title">ОБУЧАЮЩИЕ<br/>МОДУЛИ</div>

			        <div class="module__row">

				        <a href="#" class="module module_first">
					        <div class="module__wrap">
						        <div class="module__image_lg">
							        <img src="img/icon__pawn.svg" class="img-fluid" alt="">
						        </div>
						        <div class="module__status"><span>УЧЕНИК</span></div>
					        </div>
				        </a>

				        <a href="#" class="module">
					        <div class="module__wrap">
						        <div class="module__title">Презентация</div>
						        <div class="module__image">
							        <img src="img/icon__chart.svg" class="img-fluid" alt="">
						        </div>
						        <div class="module__text">до  +20 баллов</div>
						        <div class="module__rate">10 баллов</div>
					        </div>
				        </a>

				        <a href="#" class="module">
					        <div class="module__wrap">
						        <div class="module__title">Тренажер продаж</div>
						        <div class="module__image">
							        <img src="img/icon__monitor.svg" class="img-fluid" alt="">
						        </div>
						        <div class="module__text">до  +20 баллов</div>
						        <div class="module__rate">18 баллов</div>
					        </div>
				        </a>

				        <a href="#" class="module">
					        <div class="module__wrap">
						        <div class="module__title">Викторина 1<br/>Маркировка БАД</div>
						        <div class="module__image">
							        <img src="img/icon__test.svg" class="img-fluid" alt="">
						        </div>
						        <div class="module__text">до  +30 баллов</div>
						        <div class="module__rate">10 баллов</div>
					        </div>
				        </a>

				        <a href="#" class="module">
					        <div class="module__wrap">
						        <div class="module__title">Викторина 2<br/>История медицины</div>
						        <div class="module__image">
							        <img src="img/icon__test.svg" class="img-fluid" alt="">
						        </div>
						        <div class="module__text">до  +30 баллов</div>
						        <div class="module__rate">18 баллов</div>
					        </div>
				        </a>

				        <div class="module module_last">
					        <div class="module__wrap">
						        <ul class="module__social">
							        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
							        <li><a href="#"><i class="fa fa-vk"></i></a></li>
							        <li><a href="#"><i class="fa fa-google"></i></a></li>
							        <li><a href="#"><i class="fa fa-odnoklassniki"></i></a></li>
						        </ul>
						        <div class="module__subscribe">ПОДПИШИСЬ НА НАШИ СОЦСЕТИ</div>
					        </div>
				        </div>

			        </div>

		        </div>
	        </section>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
