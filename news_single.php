<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->

	        <div class="main content">
		        <div class="container">
			        <h1>Конкурс завершён! Поздравляем победителей!</h1>

			        <div class="news__image">
				        <img src="images/news_image.jpg" class="img-fluid" alt="">
			        </div>

			        <p>Завершилось главное мировое событие – Чемпионат Мира по футболу-2018! Подведены итоги и нашего футбольного Конкурса прогнозов «Кармолис и АСНА – болеем классно!», начинается самый важный и приятный этап – чествование наших победителей! Мы впечатлены всероссийским охватом нашего Конкурса – призы отправятся к своим обладателям, проживающим от Крыма до Сибири!</p>
			        <p>Завершилось главное мировое событие – Чемпионат Мира по футболу-2018! Подведены итоги и нашего футбольного Конкурса прогнозов «Кармолис и АСНА – болеем классно!», начинается самый важный и приятный этап – чествование наших победителей! Мы впечатлены всероссийским охватом нашего Конкурса – призы отправятся к своим обладателям, проживающим от Крыма до Сибири!</p>
			        <p>Завершилось главное мировое событие – Чемпионат Мира по футболу-2018! Подведены итоги и нашего футбольного Конкурса прогнозов «Кармолис и АСНА – болеем классно!», начинается самый важный и приятный этап – чествование наших победителей! Мы впечатлены всероссийским охватом нашего Конкурса – призы отправятся к своим обладателям, проживающим от Крыма до Сибири!</p>
					<br/>

			        <div class="gallery">
				        <div class="gallery__slider swiper-container">
					        <div class="swiper-wrapper">

						        <div class="swiper-slide">
							        <a href="images/gallery.jpg" data-fancybox="gallery" class="gallery">
								        <img src="images/gallery.jpg" class="img-fluid" alt="">
							        </a>
						        </div>

						        <div class="swiper-slide">
							        <a href="images/gallery.jpg" data-fancybox="gallery" class="gallery">
								        <img src="images/gallery.jpg" class="img-fluid" alt="">
							        </a>
						        </div>

						        <div class="swiper-slide">
							        <a href="images/gallery.jpg" data-fancybox="gallery" class="gallery">
								        <img src="images/gallery.jpg" class="img-fluid" alt="">
							        </a>
						        </div>

						        <div class="swiper-slide">
							        <a href="images/gallery.jpg" data-fancybox="gallery" class="gallery">
								        <img src="images/gallery.jpg" class="img-fluid" alt="">
							        </a>
						        </div>

						        <div class="swiper-slide">
							        <a href="images/gallery.jpg" data-fancybox="gallery" class="gallery">
								        <img src="images/gallery.jpg" class="img-fluid" alt="">
							        </a>
						        </div>
					        </div>
					        <!-- Add Arrows -->
					        <div class="swiper-button-next"></div>
					        <div class="swiper-button-prev"></div>
				        </div>
			        </div>



		        </div>
	        </div>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
