<!doctype html>
<html class="no-js" lang="">

	<!-- Head -->
	<?php include('inc/head.inc.php') ?>
	<!-- -->

    <body>
    
        <div class="page">

	        <!-- Header -->
            <?php include('inc/header.inc.php') ?>
	        <!-- -->

	        <div class="main">
		        <div class="container">

			        <div class="profile__heading">
				        <div class="profile__heading_title">ПУШКОВ АЛЕКСАНДР</div>
				        <div class="profile__heading_info">Баллов: <span class="color-blue">156</span></div>
			        </div>

			        <div class="profile">

				        <div class="profile__nav">
					        <div class="profile__nav_title">Меню</div>
					        <ul>
						        <li class="active"><a href="#">Общая информация</a></li>
						        <li><a href="#">Баллы</a></li>
						        <li><a href="#">Конкурс</a></li>
						        <li><a href="#">История входа</a></li>
						        <li><a href="#" class="color-red">Выход</a></li>
					        </ul>
				        </div>

				        <div class="profile__content">

					        <div class="form_group form_group_inline">
						        <div class="form_group_input offset">
							        <h3>дата и время входа</h3>
						        </div>
					        </div>

					        <div class="form_group form_group_inline">
						        <label class="inline_label inline_label_top"><span>дата входа</span></label>
						        <div class="form_group_input">
							        <ul class="profile__list">
								        <li>11.04.2018, 11:34 |  156.11.23.29</li>
								        <li>11.04.2018, 11:34 |  156.11.23.29</li>
								        <li>11.04.2018, 11:34 |  156.11.23.29</li>
								        <li>11.04.2018, 11:34 |  156.11.23.29</li>
							        </ul>
						        </div>
					        </div>
				        </div>

			        </div>

		        </div>
	        </div>

	        <!-- Footer -->
            <?php include('inc/footer.inc.php') ?>
	        <!-- -->
            
        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

    </body>
</html>
